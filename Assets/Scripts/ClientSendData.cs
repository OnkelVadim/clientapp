﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DiplomaGameServer.ViewModels.Account;

public class ClientSendData : MonoBehaviour {

    public static ClientSendData Instance;
    public Network Network;

    [Header("Login")]
    public Text LoginUsername;
    public Text LoginPassword;

    [Header("Registration")]
    public Text RegisterUsername;
    public Text RegisterPassword;
    public Text RegisterConfirmPassword;


	// Use this for initialization
	void Awake ()
    {
        Instance = this;
	}
	

    public void SendDataToServer(byte[]data)
    {
        Debug.Log(string.Format("Send data to server:{0}", data.Length));
        Network.NetworkStream.Write(data, 0, data.Length);
    }

    public void SendAccount()
    {
        if(RegisterUsername.text == string.Empty)
        {
            Debug.Log("Please insert a username.");
            return;
        }

        if(RegisterPassword.text == string.Empty)
        {
            Debug.Log("Please insert a password.");
            return;
        }

        if(RegisterConfirmPassword.text != RegisterPassword.text)
        {
            Debug.Log("Your password do not match.");
            return;
        }

        var model = new RegisterAccountView
        {
            ActionNumber = 1,
            Login = RegisterUsername.text,
            Password = RegisterPassword.text,
            ConfirmPassword = RegisterConfirmPassword.text
        };

        var json = JsonUtility.ToJson(model);
        Debug.Log(json);
        
        SendDataToServer(System.Text.Encoding.UTF8.GetBytes(json));
    }

    public void SendLogin()
    {
        if (LoginUsername.text == string.Empty)
        {
            Debug.Log("Please insert a username.");
            return;
        }

        if (LoginPassword.text == string.Empty)
        {
            Debug.Log("Please insert a password.");
            return;
        }

        var model = new LoginAccountView
        {
            ActionNumber = 2,
            Login = LoginUsername.text,
            Password = LoginPassword.text
        };
        var json = JsonUtility.ToJson(model);
        Debug.Log(json);
        SendDataToServer(System.Text.Encoding.UTF8.GetBytes(json));
    }	
}
