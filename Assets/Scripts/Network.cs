﻿using System.Net.Sockets;
using UnityEngine;
using System.IO;
using System;

public class Network : MonoBehaviour
{
    public static Network instance;

    [Header("Network Settings")]
    public string ServerIP = "127.0.0.1";
    public int ServerPort = 5500;
    public bool IsConnected;
    public bool ShouldHandleData;

    public TcpClient PlayerSocket { get; set; }
    public NetworkStream NetworkStream { get; set; }
    public StreamReader StreamReader { get; set; }
    public StreamWriter StreamWriter { get; set; }

    private byte[] _asyncBuff;
    private byte[] _myBytes;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        ConnectGameServer();
    }

    void ConnectGameServer()
    {
        if (PlayerSocket != null)
        {
            if (PlayerSocket.Connected || IsConnected)
                return;
            PlayerSocket.Close();
            PlayerSocket = null;
        }

        PlayerSocket = new TcpClient()
        {
            ReceiveBufferSize = 4096,
            SendBufferSize = 4096,
            NoDelay = false
        };

        Array.Resize(ref _asyncBuff, 8192);

        PlayerSocket.BeginConnect(ServerIP, ServerPort, new AsyncCallback(ConnectCallback), PlayerSocket);
        IsConnected = true;
        MenuManager.Instance.MenuState = MenuManager.Menu.Login;
    }

    void ConnectCallback(IAsyncResult result)
    {
        if (PlayerSocket != null)
        {
            PlayerSocket.EndConnect(result);
            if (PlayerSocket.Connected == false)
            {
                IsConnected = false;
                return;
            }
            else
            {
                PlayerSocket.NoDelay = true;
                NetworkStream = PlayerSocket.GetStream();
                NetworkStream.BeginRead(_asyncBuff, 0, 8192, OnReceive, null);
            }
        }
    }

    private void Update()
    {
        if (ShouldHandleData)
        {
            ClientHandleData.Instance.HandleData(_myBytes);
            ShouldHandleData = false;
        }
    }

    void OnReceive(IAsyncResult result)
    {
        if (PlayerSocket != null)
        {
            if (PlayerSocket == null)
                return;

            int byteArray = NetworkStream.EndRead(result);
            _myBytes = null;
            Array.Resize(ref _myBytes, byteArray);
            Buffer.BlockCopy(_asyncBuff, 0, _myBytes, 0, byteArray);

            if (byteArray == 0)
            {
                Debug.Log("You got disconnected from the Server.");
                PlayerSocket.Close();
                return;
            }

            ShouldHandleData = true;

            if (PlayerSocket == null)
                return;

            NetworkStream.BeginRead(_asyncBuff, 0, 8192, OnReceive, null);
        }
    }
}
