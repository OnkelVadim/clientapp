﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public static MenuManager Instance;

    [Header("Menu GameObjects")]
    public GameObject LoadingMenu;
    public GameObject LoginMenu;
    public GameObject RegisterMenu;
    public GameObject HomeMenu;

    public Menu MenuState;
    public enum Menu
    {
        Loading = 1,
        Login = 2,
        Register = 3,
        Home = 4
    }

    private void Awake()
    {
        Instance = this;
        MenuState = Menu.Loading;
    }

    void Update ()
    {
		switch(MenuState)
        {
            case Menu.Loading:
                LoadingMenu.SetActive(true);
                LoginMenu.SetActive(false);
                RegisterMenu.SetActive(false);
                HomeMenu.SetActive(false);
                break;
            case Menu.Login:
                LoadingMenu.SetActive(false);
                LoginMenu.SetActive(true);
                RegisterMenu.SetActive(false);
                HomeMenu.SetActive(false);
                break;
            case Menu.Register:
                LoadingMenu.SetActive(false);
                LoginMenu.SetActive(false);
                RegisterMenu.SetActive(true);
                HomeMenu.SetActive(false);
                break;
            case Menu.Home:
                LoadingMenu.SetActive(false);
                LoginMenu.SetActive(false);
                RegisterMenu.SetActive(false);
                HomeMenu.SetActive(true);
                break;
        }
	}

    public void ChangeMenu(int menu)
    {
        MenuState = (Menu)menu;
    }
}
