﻿using DiplomaGameServer.ViewModels;
using DiplomaGameServer.ViewModels.Account;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class ClientHandleData : MonoBehaviour
{
    public static ClientHandleData Instance;

    [Header("Player Menu")]
    public Text PlayerWelcome;

    private Dictionary<int, Packet> _packets;
    private void Awake()
    {
        Instance = this;
        _packets = new Dictionary<int, Packet>();
        _packets.Add(1, HandleRegister);
        _packets.Add(2, HandleLogin);
    }

    public void HandleData(byte[] data)
    {
        try
        {
            var requestString = System.Text.Encoding.UTF8.GetString(data);
            var requestData = JsonUtility.FromJson<ResponseBaseView>(requestString);

            if(!requestData.Result)
            {
                Debug.Log(string.Format("Error: {0}", requestData.Message));
                return;
            }

            Packet packet;
            if (_packets.TryGetValue(requestData.ActionNumber, out packet))
            {
                packet.Invoke(requestString);
                return;
            }

            Debug.Log(string.Format("Handler is not exist!: {0}", requestData.ActionNumber));
        }
        catch(Exception ex)
        {
            Debug.Log(string.Format("Handle error: {0}", ex.Message));
        }
    }

    void HandleLogin(string jsonData)
    {
        var result = JsonUtility.FromJson<LoginAccountResponseView>(jsonData);

        Player.instance.PlayerInfo = result.PlayerInfo;

        PlayerWelcome.text = "Welcome back!";
        MenuManager.Instance.MenuState = MenuManager.Menu.Home;
    }

    void HandleRegister(string jsonData)
    {
        var result = JsonUtility.FromJson<RegisterAccountResponseView>(jsonData);

        Player.instance.PlayerInfo = result.PlayerInfo;

        PlayerWelcome.text = "Welcome back!";
        MenuManager.Instance.MenuState = MenuManager.Menu.Home;
    }

    private delegate void Packet(string jsonData);
}
